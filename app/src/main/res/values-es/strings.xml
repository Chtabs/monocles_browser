<?xml version="1.0" encoding="utf-8"?>

<!--
  Copyright © 2018-2021 Arne-Brün Vogelsang <arne@monocles.de>.

  Translation 2017-2021 Jose A. León Becerra.  Copyright assigned to Soren Stoutner <soren@stoutner.com>.

  This file is part of monocles browser <https://monocles.de/more>. It is a
 * fork of Privacy Browser, which is Copyright © 2015-2021 Soren Stoutner
 * <soren@stoutner.com>.

  monocles browser is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  monocles browser is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with monocles browser.  If not, see <http://www.gnu.org/licenses/>. -->

<resources>
    <!-- Activities. -->
    <string name="monocles_browser">monocles browser</string>
    <string name="short_name">browser</string>
    <!-- For translations, `android_asset_path` should be the localization abbreviation.  This should not be translated unless the Guide and About sections are localized. -->
    <string name="android_asset_path">es</string>

    <!-- MainWebViewActivity. -->
    <string name="privacy_mode">Modo Privado</string>
    <string name="javascript_enabled">Javascript habilitado</string>
    <string name="javascript_disabled">Javascript deshabilitado</string>
    <string name="cookies_enabled">Cookies habilitadas</string>
    <string name="cookies_disabled">Cookies deshabilitadas</string>
    <string name="dom_storage_enabled">Almacenamiento DOM habilitado</string>
    <string name="dom_storage_disabled">Almacenamiento DOM deshabilitado</string>
    <string name="form_data_enabled">Datos de formulario habilitados</string>
    <string name="form_data_disabled">Datos de fromulario deshabilitados</string>
    <string name="cookies_deleted">Cookies borradas</string>
    <string name="dom_storage_deleted">Almacenamiento DOM borrado</string>
    <string name="form_data_deleted">Datos de formulario borrado</string>
    <string name="open_navigation_drawer">Abrir la caja de navegación</string>
    <string name="close_navigation_drawer">Cerrar la caja de navegación</string>
    <string name="unrecognized_url">URL no reconocida:</string>
    <string name="add_tab">Añadir pestaña</string>
    <string name="close_tab">Cerrar pestaña</string>
    <string name="new_tab">Nueva pestaña</string>
    <string name="loading">Cargando…</string>
    <string name="error">Error:</string>
    <string name="apply">Apply</string>

    <!-- Loading Blocklists. -->
    <string name="loading_easylist">Cargando EasyList</string>
    <string name="loading_easyprivacy">Cargando EasyPrivacy</string>
    <string name="loading_fanboys_annoyance_list">Cargando la lista molesta de Fanboy</string>
    <string name="loading_fanboys_social_blocking_list">Cargando la lista de bloqueo social de Fanboy</string>
    <string name="loading_ultralist">Cargando UltraList</string>
    <string name="loading_blackhosts">Cargando Blackhosts</string>

    <!-- Custom App Bar. -->
    <string name="favorite_icon">Icono favorito</string>
    <string name="url_or_search_terms">URL o búsqueda</string>

    <!-- View SSL Certificate. -->
    <string name="view_ssl_certificate">Ver certificado SSL</string>
    <string name="unencrypted_website">Web sin cifrar</string>
    <string name="no_ssl_certificate">La comunicación con esta página web no está cifrada.
        Esto permite a terceras partes interceptar información, rastrear su navegación e inyectar contenido malicioso.</string>
    <string name="ssl_certificate">Certificado SSL</string>
    <string name="close">Cerrar</string>
    <string name="domain">Dominio</string>
    <string name="domain_label">Dominio:</string>
    <string name="ip_addresses">Direcciones IP:</string>
    <string name="issued_to">Emitido a</string>
    <string name="issued_by">Emitido por</string>
    <string name="common_name">Nombre Común (CN):</string>
    <string name="organization">Organización (O):</string>
    <string name="organizational_unit">Unidad Organizativa (OU):</string>
    <string name="valid_dates">Periodo de validez</string>
    <string name="start_date">Comienza el:</string>
    <string name="end_date">Caduca el:</string>

    <!-- SSL Certificate Error. -->
    <string name="ssl_certificate_error">Error de certificado SSL</string>
    <string name="proceed">Proceder</string>
    <string name="future_certificate">La fecha inicio del certificado está en el futuro</string>
    <string name="expired_certificate">El certificado ha expirado</string>
    <string name="cn_mismatch">El nombre común no es iguál al nombre del host</string>
    <string name="untrusted">La autoridad certificada no es confiable</string>
    <string name="invalid_date">La fecha del certificado es inválida</string>
    <string name="invalid_certificate">El certificado es inválido</string>
    <string name="url">URL</string>
    <string name="url_label">URL:</string>

    <!-- Pinned Mismatch. -->
    <string name="pinned_mismatch">Desajuste fijado</string>
    <string name="update">Actualizar</string>
    <string name="current">Actual</string>
    <string name="pinned">Fijado</string>

    <!-- HTTP Authentication. -->
    <string name="http_authentication">Autenticación HTTP</string>
    <string name="host">Host:</string>
    <string name="username">Usuario</string>
    <string name="password">Contraseña</string>

    <!-- MainWebViewActivity Navigation Menu. -->
    <string name="navigation_drawer">Caja de navegación</string>
    <string name="clear_and_exit">Borrar y salir</string>
    <string name="home">Inicio</string>
    <string name="back">Atrás</string>
    <string name="forward">Adelante</string>
    <string name="history">Historial</string>
        <string name="clear_history">Borrar historial</string>
    <string name="open">Abrir</string>
    <string name="downloads">Descargas</string>
        <string name="no_file_manager_detected">El sistema no puede detectar un gestor de archivos compatible.</string>
    <string name="settings">Configuración</string>
    <string name="import_export">Importar/Exportar</string>
    <string name="logcat">Logcat</string>
    <string name="guide">Guía</string>
    <string name="about">Acerca de</string>

    <!-- MainWebViewActivity Options Menu. -->
    <string name="javascript">Javascript</string>
    <string name="refresh">Actualizar</string>
    <string name="stop">Parar</string>
    <string name="cookies">Cookies</string>
    <string name="dom_storage">Almacenamiento DOM</string>
    <string name="form_data">Datos de formulario</string>  <!-- The form data strings can be removed once the minimum API >= 26. -->
    <string name="clear_data">Borrar datos</string>
        <string name="clear_cookies">Borrar cookies</string>
        <string name="clear_dom_storage">Borrar almacenamiento DOM</string>
        <string name="clear_form_data">Borrar datos de formulario</string>  <!-- The form data strings can be removed once the minimum API >= 26. -->
        <string name="options_fanboys_annoyance_list">Lista molesta de Fanboy</string>
        <string name="options_fanboys_social_blocking_list">Lista de bloqueo social de Fanboy</string>
        <string name="options_block_all_third_party_requests">Bloquear todas las solicitudes de terceros</string>
    <string name="page">Página</string>
        <string name="options_user_agent">Agente de usuario</string>
            <string name="user_agent_monocles_browser">monocles browser</string>
            <string name="user_agent_webview_default">WebView por defecto</string>
            <string name="user_agent_firefox_on_android">Firefox en Android</string>
            <string name="user_agent_chrome_on_android">Chrome en Android</string>
            <string name="user_agent_safari_on_ios">Safari en iOS</string>
            <string name="user_agent_firefox_on_linux">Firefox en Linux</string>
            <string name="user_agent_chromium_on_linux">Chromium en Linux</string>
            <string name="user_agent_firefox_on_windows">Firefox en Windows</string>
            <string name="user_agent_chrome_on_windows">Chrome en Windows</string>
            <string name="user_agent_edge_on_windows">Edge en Windows</string>
            <string name="user_agent_internet_explorer_on_windows">Internet Explorer en Windows</string>
            <string name="user_agent_safari_on_macos">Safari en macOS</string>
            <string name="user_agent_custom">Personalizado</string>
        <string name="swipe_to_refresh_options_menu">Deslizar para actualizar</string>
        <string name="wide_viewport">Vista amplia</string>
        <string name="display_images">Mostrar imágenes</string>
        <string name="dark_webview">WebView oscuro</string>
        <string name="font_size">Tamaño de fuente</string>
        <string name="find_on_page">Buscar en página</string>
        <string name="print">Imprimir</string>
            <string name="monocles_browser_webpage">Página web de monocles browser</string>
        <string name="save">Guardar</string>
        <string name="add_to_home_screen">Añadir a la ventana de inicio</string>
        <string name="view_source">Ver la fuente</string>
    <string name="share">Compartir</string>
        <string name="share_url">Compartir URL</string>
        <string name="open_with_app">Abrir con App</string>
        <string name="open_with_browser">Abrir con Navegador</string>
    <string name="add_domain_settings">Añadir dominio</string>
    <string name="edit_domain_settings">Editar dominio</string>

    <!-- Context Menus. -->
    <string name="open_in_new_tab">Abrir en nueva pestaña</string>
    <string name="open_in_background">Abrir en segundo plano</string>
    <string name="open_image_in_new_tab">Abrir imagen en nueva pestaña</string>
    <string name="copy_url">Copiar URL</string>
    <string name="email_address">Correo electrónico</string>
    <string name="copy_email_address">Copiar correo electrónico</string>
    <string name="write_email">Escribir correo electrónico</string>
    <string name="view_image">Ver imagen</string>

    <!-- Find on Page. -->
    <string name="previous">Anterior</string>
    <string name="next">Siguiente</string>

    <!-- Open Dialog. -->
    <string name="file_is_mht">El archivo es un archivo de web MHT.</string>
    <string name="mht_checkbox_explanation">A veces se necesita especificar manualmente los archivos web MIME Encapsulated HTML (MHT) para que se abran correctamente.</string>

    <!-- Save Dialogs. -->
    <string name="save_url">Guardar URL</string>
    <string name="save_archive">Guardar archivo</string>
    <string name="save_text">Guardar texto</string>
    <string name="save_image">Guardar imagen</string>
    <string name="save_logcat">Guardar logcat</string>
    <string name="file_name">Nombre de archivo</string>
    <string name="monocles_browser_logcat_txt">monocles browser Logcat.txt</string>
    <string name="monocles_browser_version_txt">Versión de monocles browser.txt</string>
    <string name="monocles_browser_version_png">Versiótoutnern de monocles browser.png</string>
    <string name="file">Archivo</string>
    <string name="bytes">bytes</string>
    <string name="unknown_size">Tamaño desconocido</string>
    <string name="invalid_url">URL inválida</string>
    <string name="saving_file">Guardando archivo:</string>
    <string name="file_saved">Archivo guardado:</string>
    <string name="processing_image">Procesando imagen… :</string>
    <string name="image_saved">Imagen guardada:</string>
    <string name="error_saving_file">Error guardando archivo:</string>

    <!-- View Source. -->
    <string name="request_headers">Cabeceras de solicitud</string>
    <string name="response_message">Mensaje de respuesta</string>
    <string name="response_headers">Cabeceras de respuesta</string>
    <string name="response_body">Cuerpo de respuesta</string>
    <string name="content_metadata">Metadatos del contenido</string>
    <string name="content_data">Datos del contenido</string>
    <string name="untrusted_ssl_certificate">El certificado SSL no es de confianza.</string>
    <string name="load_anyway">Cargar de todos modos</string>
    <string name="about_view_source">Acerca de ver la fuente</string>
    <string name="about_view_source_message">Debido a que WebView de Android no expone la información fuente,
        se hizo una solicitud por separado utilizando las herramientas del sistema para recopilar la información mostrada en esta actividad.
        Puede haber algunas diferencias entre estos datos y los utilizados por WebView en la actividad principal.
        Esta limitación se eliminará en la serie 4.x con el lanzamiento de Privacy WebView.</string>

    <!-- Create Home Screen Shortcut Alert Dialog. -->
    <string name="create_shortcut">Crear acceso directo</string>
    <string name="shortcut_name">Nombre de acceso directo</string>
    <string name="open_with_default_browser">Abrir con navegador por defecto.</string>
    <string name="open_with_monocles_browser">Abrir con monocles browser.</string>
    <string name="cancel">Cancelar</string>
    <string name="create">Crear</string>

    <!-- Bookmarks. -->
    <string name="bookmarks">Favoritos</string>
    <string name="database_view">Vista de base de datos</string>
    <string name="bookmark_opened_in_background">El favorito se abrió en una pestaña de fondo.</string>
    <string name="create_bookmark">Crear favorito</string>
    <string name="create_folder">Crear carpeta</string>
    <string name="current_bookmark_icon">Icono de favorito actual</string>
    <string name="current_folder_icon">Icono de carpeta actual</string>
    <string name="default_folder_icon">Icono de carpeta por defecto</string>
    <string name="webpage_favorite_icon">Icono favorito de página Web</string>
    <string name="bookmark_name">Nombre de favorito</string>
    <string name="folder_name">Nombre de carpeta</string>
    <string name="bookmark_url">Favorito URL</string>
    <string name="folder_names_must_be_unique">Los nombres de carpetas deben ser únicos</string>
    <string name="edit_bookmark">Editar favorito</string>
    <string name="edit_folder">Editar carpeta</string>
    <string name="move_to_folder">Mover a carpeta</string>
    <string name="move">Mover</string>

    <!-- Bookmarks Contextual App Bar. -->
    <string name="selected">Seleccionados:</string>  <!--This is a plural adjective.-->
    <string name="move_up">Subir</string>
    <string name="move_down">Bajar</string>
    <string name="edit">Editar</string>
    <string name="delete">Borrar</string>
    <string name="select_all">Selectionar todo</string>
    <string name="bookmarks_deleted">Favoritos borrados:</string>
    <string name="undo">Deshacer</string>

    <!-- Bookmarks Database View. -->
    <string name="bookmarks_database_view">Vista de base de datos de favoritos</string>
    <string name="all_folders">Todas las carpetas</string>
    <string name="home_folder">Carpeta inicio</string>
    <string name="sort">Ordenar</string>
        <string name="sorted_by_database_id">Ordenados por ID de base de datos.</string>
        <string name="sorted_by_display_order">Ordenados por orden de visualización.</string>
    <string name="database_id">ID de base de datos:</string>
    <string name="folder">Carpeta:</string>
    <string name="parent_folder">Carpeta superior:</string>
    <string name="display_order">Mostrar orden:</string>
    <string name="cannot_deselect_bookmark">No se puede deseleccionar un favorito mientras la carpeta superior está seleccionada.</string>

    <!-- Requests. -->
    <string name="requests">Peticiones</string>
    <string name="request_details">Detalles de petición</string>
    <string name="disposition">Disposición</string>
        <string name="all">Todas</string>
        <string name="default_label">Por defecto</string>
        <string name="default_allowed">Por defecto - Permitida</string>
        <string name="allowed">Permitida</string>
        <string name="allowed_plural">Permitidas</string>
        <string name="third_party_plural">Terceras partes</string>
        <string name="third_party_blocked">Tercera parte - Bloqueada</string>
        <string name="blocked">Bloqueada</string>
        <string name="blocked_plural">Bloqueadas</string>
    <string name="blocklist">Lista de bloqueo</string>
    <string name="sublist">Sublista</string>
        <string name="main_whitelist">Lista blanca principal</string>
        <string name="final_whitelist">Lista blanca final</string>
        <string name="domain_whitelist">Lista blanca de dominios</string>
        <string name="domain_initial_whitelist">Lista blanca inicial de dominios</string>
        <string name="domain_final_whitelist">Lista blanca final de dominios</string>
        <string name="third_party_whitelist">Lista blanca de terceros</string>
        <string name="third_party_domain_whitelist">Lista blanca de dominios de terceros</string>
        <string name="third_party_domain_initial_whitelist">Lista blanca inicial de dominios de terceros</string>
        <string name="main_blacklist">Lista negra principal</string>
        <string name="initial_blacklist">Lista negra inicial</string>
        <string name="final_blacklist">Lista negra final</string>
        <string name="domain_blacklist">Lista negra de dominios</string>
        <string name="domain_initial_blacklist">Lista negra inicial de dominios</string>
        <string name="domain_final_blacklist">Lista negra final de dominios</string>
        <string name="domain_regular_expression_blacklist">Lista negra de expresiones regulares de dominios</string>
        <string name="third_party_blacklist">Lista negra de terceros</string>
        <string name="third_party_initial_blacklist">Lista negra inicial de terceros</string>
        <string name="third_party_domain_blacklist">Lista negra de dominios de terceros</string>
        <string name="third_party_domain_initial_blacklist">Lista negra inicial de dominios de terceros</string>
        <string name="third_party_regular_expression_blacklist">Lista negra de expresiones regulares de terceros</string>
        <string name="third_party_domain_regular_expression_blacklist">Lista negra de expresiones regulares de dominios de terceros</string>
        <string name="regular_expression_blacklist">Lista negra de expresiones regulares</string>
    <string name="blocklist_entries">Entradas de la lista de bloqueo</string>
    <string name="blocklist_original_entry">Entrada original de la lista de bloqueo</string>

    <!-- Domains. -->
    <string name="domains">Dominios</string>
    <string name="domain_settings">Configuración de dominio</string>
    <string name="add_domain">Añadir dominio</string>
    <string name="domain_name_already_exists">El nombre de dominio ya existe</string>
    <string name="add">Añadir</string>
    <string name="domain_name">Nombre de dominio</string>
    <string name="domain_deleted">Dominio borrado</string>
    <string name="domain_name_instructions">*. puede ser añadido a un dominio para incluir todos los subdominios (p.ej. *.monocles.de)</string>
    <string-array name="font_size_array">
        <item>Por defecto del sistema</item>
        <item>Tamaño de letra personalizado</item>
    </string-array>
    <string-array name="swipe_to_refresh_array">
        <item>Por defecto del sistema</item>
        <item>Deslizar para actualizar habilitado</item>
        <item>Deslizar para actualizar deshabilitado</item>
    </string-array>
    <string-array name="webview_theme_array">
        <item>Por defecto del sistema</item>
        <item>Tema de WebView claro</item>
        <item>Tema de WebView oscuro</item>
    </string-array>
    <string-array name="wide_viewport_array">
        <item>Por defecto del sistema</item>
        <item>Vista amplia habilitada</item>
        <item>Vista amplia deshabilitada</item>
    </string-array>
    <string-array name="display_webpage_images_array">
        <item>Por defecto del sistema</item>
        <item>Imágenes habilitadas</item>
        <item>Imágenes deshabilitadas</item>
    </string-array>
    <string name="pinned_ssl_certificate">Certificado SSL fijado</string>
        <string name="saved_ssl_certificate">Certificado SSL guardado</string>
        <string name="current_website_ssl_certificate">Certificado SSL actual de la web</string>
        <string name="load_an_encrypted_website">Cargar una página web cifrada antes de abrir la configuración de dominio para rellenar el certificado SSL de la página web actual.</string>
    <string name="pinned_ip_addresses">Direcciones IP fijadas</string>
        <string name="saved_ip_addresses">Direcciones IP guardadas</string>
        <string name="current_ip_addresses">Direcciones IP actuales</string>

    <!-- Import/Export. -->
    <string name="encryption">Cifrado</string>
    <string-array name="encryption_type">
        <item>Ninguno</item>
        <item>Contraseña</item>
        <item>OpenPGP</item>
    </string-array>
    <string name="kitkat_password_encryption_message">El cifrado de contraseñas no funciona en Android KitKat.</string>
    <string name="openkeychain_required">El cifrado OpenPGP requiere que esté instalado OpenKeychain.</string>
    <string name="openkeychain_import_instructions">El archivo sin cifrar tendrá que ser importado en un paso separado después de ser descifrado.</string>
    <string name="file_location">Ubicación del archivo</string>
    <string name="browse">Navegar</string>
    <string name="export">Exportar</string>
    <string name="import_button">Importar</string>  <!-- `import` is a reserved word and cannot be used as the name. -->
    <string name="decrypt">Descifrar</string>
    <string name="export_successful">Exportación exitosa.</string>
    <string name="export_failed">Exportación fallida:</string>
    <string name="import_failed">Importación fallida:</string>

    <!-- Logcat. -->
    <string name="copy_string">Copiar</string>  <!-- `copy` is a reserved word and should not be used as the name. -->
    <string name="logcat_copied">Logcat copiado.</string>
    <string name="clear">Borrar</string>

    <!-- Guide. -->
    <string name="overview">Visión general</string>
    <string name="local_storage">Almacenamiento local</string>
    <string name="ssl_certificates">Certificados SSL</string>
    <string name="proxies">Proxis</string>
    <string name="tracking_ids">Rastreo de IDs</string>

    <!-- Proxy. -->
    <string name="orbot_not_installed_title">Orbot No Instalado</string>
    <string name="orbot_not_installed_message">El proxy a través de Orbot no funcionará a menos que la aplicación Orbot esté instalada.</string>
    <string name="i2p_not_installed_title">I2P No Instalado</string>
    <string name="i2p_not_installed_message">El proxy a través de I2P no funcionará a menos que la aplicación I2P esté instalada.</string>
    <string name="waiting_for_orbot">Esperando a que Orbot se conecte.</string>
    <string name="custom_proxy_invalid">La URL del proxy personalizado no es válida.</string>
    <string name="socks_proxies_do_not_work_on_kitkat">SOCKS proxies do not work on Android KitKat.</string>

    <!-- About Activity. -->
    <string name="about_monocles_browser">Acerca de monocles browser</string>
    <string name="version">Versión</string>
        <string name="version_code">código de versión</string>
        <string name="hardware">Hardware</string>
            <string name="brand">Marca:</string>
            <string name="manufacturer">Fabricante:</string>
            <string name="model">Modelo:</string>
            <string name="device">Dispositivo:</string>
            <string name="bootloader">Cargador de arranque:</string>
            <string name="radio">Radio:</string>
        <string name="software">Software</string>
            <string name="android">Android:</string>
            <string name="api">API</string>
            <string name="build">Versión de compilación:</string>
            <string name="security_patch">Parche de seguridad:</string>
            <string name="webview_provider">Proveedor de WebView:</string>
            <string name="webview_version">Versión de WebView:</string>
            <string name="orbot">Orbot:</string>
            <string name="i2p">I2P:</string>
            <string name="openkeychain">OpenKeychain:</string>
        <string name="memory_usage">Uso de memoria</string>
            <string name="app_consumed_memory">Memoria conumida de la app:</string>
            <string name="app_available_memory">Memoria disponible de la app:</string>
            <string name="app_total_memory">Memoria total de la app:</string>
            <string name="app_maximum_memory">Memoria máxima de la app:</string>
            <string name="system_consumed_memory">Memoria consumida del sistema:</string>
            <string name="system_available_memory">Memoria disponible del sistema:</string>
            <string name="system_total_memory">Memoria total del sistema:</string>
            <string name="mebibyte">MiB</string>
            <string name="easylist_label">EasyList:</string>
            <string name="easyprivacy_label">EasyPrivacy:</string>
            <string name="fanboy_annoyance_label">Lista molesta de Fanboy:</string>
            <string name="fanboy_social_label">Lista de bloqueo social de Fanboy:</string>
            <string name="ultralist_label">UltraList:</string>
            <string name="ultraprivacy_label">Ultra Privacidad:</string>
    <string name="blackhosts_label">BlackHosts:</string>
    <string name="package_signature">Firma del paquete</string>
            <string name="issuer_dn">DN del emisor:</string>
            <string name="subject_dn">DN del sujeto:</string>
            <string name="certificate_version">Versión del certificado:</string>
            <string name="serial_number">Número de serie:</string>
            <string name="signature_algorithm">Algoritmo de firma:</string>
        <string name="version_info_copied">Información de la versión copiada.</string>
    <string name="permissions">Permisos</string>
    <string name="privacy_policy">Política de privacidad</string>
    <string name="changelog">Historial de cambios</string>
    <string name="licenses">Licencias</string>
    <string name="contributors">Colaboradores</string>
    <string name="links">Enlaces</string>

    <!-- Preferences. -->
    <string name="privacy">Privacidad</string>
        <string name="javascript_preference_summary">Javascript permite a las webs ejecutar programas (scripts) en el dispositivo.</string>
        <string name="cookies_preference_summary">Las cookies son un ajuste a nivel de aplicación.
            Cuando la pestaña activa tiene las cookies habilitadas, cualquier solicitud de red realizada en segundo plano por otra pestaña también tiene las cookies habilitadas.</string>
        <string name="dom_storage_preference">Almacenamiento DOM</string>
        <string name="dom_storage_preference_summary">JavaScript debe estar activado para que el almacenamiento del Modelo de Objetos del Documento funcione.</string>
        <string name="save_form_data_preference">Datos de formulario</string>
        <string name="save_form_data_preference_summary">Los datos de formularios guardados pueden rellenar automáticamente las casillas o campos de las webs.</string>
        <string name="user_agent">Agente de usuario</string>
        <string-array name="translated_user_agent_names">
            <item>monocles browser</item>
            <item>WebView por defecto</item>
            <item>Firefox en Android</item>
            <item>Chrome en Android</item>
            <item>Safari en iOS</item>
            <item>Firefox en Linux</item>
            <item>Chromium en Linux</item>
            <item>Firefox en Windows</item>
            <item>Chrome en Windows</item>
            <item>Edge en Windows</item>
            <item>Internet Explorer en Windows</item>
            <item>Safari en macOS</item>
            <item>Personalizado</item>
        </string-array>
        <string-array name="translated_domain_settings_user_agent_names">  <!-- The translated names of the user agents with a System Default option for the domains spinner. -->
            <item>Por defecto del sistema</item>
            <item>monocles browser</item>
            <item>WebView por defecto</item>
            <item>Firefox en Android</item>
            <item>Chrome en Android</item>
            <item>Safari en iOS</item>
            <item>Firefox en Linux</item>
            <item>Chromium en Linux</item>
            <item>Firefox en Windows</item>
            <item>Chrome en Windows</item>
            <item>Edge en Windows</item>
            <item>Internet Explorer en Windows</item>
            <item>Safari en macOS</item>
            <item>Personalizado</item>
        </string-array>
        <string name="custom_user_agent">Agente de usuario personalizado</string>
        <string name="incognito_mode">Modo incógnito</string>
        <string name="incognito_mode_summary">Borrar el historial y el caché después de que cada página web termine de cargar.
            En el modo Incógnito, Atrás cierra la pestaña (o la app si hay solo una pestaña).</string>
        <string name="allow_screenshots">Permitir capturas de pantalla</string>
        <string name="allow_screenshots_summary">Permitir capturas de pantalla, grabación de vídeo y visualización en pantallas inseguras. Cambiar esta configuración reiniciará monocles browser.</string>
    <string name="blocklists">Listas de bloqueo</string>
        <string name="easylist">EasyList</string>
        <string name="easylist_summary">Lista principal de bloqueo de anuncios.</string>
        <string name="easyprivacy">EasyPrivacy</string>
        <string name="easyprivacy_summary">Lista principal de bloqueo de rastreadores.</string>
        <string name="fanboys_annoyance_list">Lista molesta de Fanboy</string>
        <string name="fanboys_annoyance_list_summary">Bloquear popups y enlaces molestos.  Incluye la lista de bloqueo social de Fanboy.</string>
        <string name="fanboys_social_blocking_list">Lista de bloqueo social de Fanboy</string>
        <string name="fanboys_social_blocking_list_summary">Bloquear contenidos de medios sociales de terceros.</string>
        <string name="ultralist">UltraList</string>
        <string name="ultralist_summary">UltraList bloquea los anuncios que EasyList no bloquea, porque al hacerlo puede romper páginas web.</string>
        <string name="ultraprivacy">Ultra Privacidad</string>
    <string name="blackhosts">BlackHosts</string>
    <string name="ultraprivacy_summary">Ultra Privacidad bloquea los rastreadores que no bloquea EasyPrivacy, porque al hacerlo puede romper páginas web.</string>
        <string name="block_all_third_party_requests">Bloquear todas las solicitudes de terceras partes</string>
        <string name="block_all_third_party_requests_summary">Bloquear todas las solicitudes de terceras  partes aumenta la privacidad, pero rompe muchas páginas web.</string>
    <string name="url_modification">Modificación de URL</string>
        <string name="google_analytics">Google Analytics</string>
        <string name="google_analytics_summary">Eliminar “?utm_”, “&amp;utm_” y cualquier cosa tras ello de las URLs.</string>
        <string name="facebook_click_ids">IDs de clics en Facebook</string>
        <string name="facebook_click_ids_summary">Eliminar “?fbclid=”, “&amp;fbclid=”, “?fbadid=”, “&amp;fbadid=” y cualquier cosa tras ello de las URLs.</string>
        <string name="twitter_amp_redirects">Redirecciones de Twitter AMP</string>
        <string name="twitter_amp_redirects_summary">Eliminar “?amp=1” y cualquier cosa después de esto de las URLs.</string>
    <string name="search">Búsqueda</string>
        <string-array name="search_entries">
            <item>Startpage</item>
            <item>Metager</item>
            <item>Mojeek</item>
            <item>DuckDuckGo - Javascript deshabilitado</item>
            <item>DuckDuckGo - Javascript habilitado</item>
            <item>Google</item>
            <item>Bing</item>
            <item>Yahoo - Javascript deshabilitado</item>
            <item>Yahoo - Javascript habilitado</item>
            <item>Personalizado</item>
        </string-array>
        <string name="custom_url">URL personalizado</string>
        <string name="search_custom_url">URL personalizado de búsqueda</string>
    <string name="proxy">Proxy</string>
        <string name="proxy_none">Ninguno</string>
        <string name="proxy_tor">Tor</string>
        <string name="proxy_i2p">I2P</string>
        <string name="proxy_custom">Personalizado</string>
        <string-array name="proxy_entries">
            <item>Ninguno</item>
            <item>Tor</item>
            <item>I2P</item>
            <item>Personalizado</item>
        </string-array>
        <string name="no_proxy_enabled">Ninguno - conectar directamente a Internet.</string>
        <string name="tor_enabled">Tor - conectar a través de socks://localhost:9050.</string>
        <string name="tor_enabled_kitkat">Tor - conectar a través de http://localhost:8118.</string>
        <string name="i2p_enabled">I2P - conectar a través de http://localhost:4444.</string>
        <string name="custom_proxy">Proxy personalizado</string>
    <string name="proxy_custom_url">URL personalizada del proxy</string>
    <string name="full_screen">Pantalla completa</string>
        <string name="full_screen_browsing_mode">Navegación de pantalla completa</string>
        <string name="full_screen_browsing_mode_summary">Doble toque para alternar a modo de navegación de pantalla completa.</string>
        <string name="hide_app_bar">Ocultar la barra de aplicaciones</string>
        <string name="hide_app_bar_summary">Ocultar la barra de aplicaciones que contiene la URL.</string>
    <string name="clear_everything">Borrar todo</string>
        <!-- The form data part of this string can be removed once the minimum API >= 26. -->
        <string name="clear_everything_summary">Borra cookies, almacenamiento DOM, datos de formulario, el logcat y la caché de  WebView.
            A continuación borra manualmente los directorios “app_webview” y “cache”.</string>
        <string name="clear_cookies_preference">Borrar cookies</string>
        <string name="clear_cookies_summary">Borra las cookies.</string>
        <string name="clear_dom_storage_preference">Borrar almacenamiento DOM</string>
        <string name="clear_dom_storage_summary">Borra el almacenamiento DOM.</string>
        <string name="clear_form_data_preference">Borrar datos de formulario</string>  <!-- The form data strings can be removed once the minimum API >= 26. -->
        <string name="clear_form_data_summary">Borra los datos de formulario.</string>  <!-- The form data strings can be removed once the minimum API >= 26. -->
        <string name="clear_logcat_preference">Borrar logcat</string>
        <string name="clear_logcat_summary">Borra el logcat.</string>
        <string name="clear_cache">Borrar caché</string>
        <string name="clear_cache_summary">Borra la caché de WebView.</string>
    <string name="general">General</string>
        <string name="homepage">Página de inicio</string>
        <string name="font_size_preference">Tamaño de fuente</string>
        <string name="open_intents_in_new_tab">Abrir contenido en nueva pestaña</string>
        <string name="open_intents_in_new_tab_summary">Los contenidos son enlaces enviados desde otras apps.</string>
        <string name="swipe_to_refresh">Deslizar para actualizar</string>
        <string name="swipe_to_refresh_summary">Algunas webs no funcionan bien si la opción deslizar para actualizar está habilitada.</string>
        <string name="download_with_external_app">Descargar con una app externa</string>
        <string name="download_with_external_app_summary">Use una app externa para descargar archivos.</string>
        <string name="scroll_app_bar">Desplazar la barra de aplicaciones</string>
        <string name="scroll_app_bar_summary">Desplazar la barra de aplicaciones desde la parte superior de la pantalla cuando el WebView se desplaza hacia abajo.</string>
        <string name="display_additional_app_bar_icons">Mostrar iconos adicionales en la barra de aplicación</string>
        <string name="display_additional_app_bar_icons_summary">Mostrar iconos en la barra de aplicaciones para refrescar el WebView y, si hay espacio,
            para abrir el cajón de marcadores y cambiar las cookies.</string>
        <string name="app_theme">Tema de la app</string>
        <string-array name="app_theme_entries">
            <item>Por defecto del sistema</item>
            <item>Claro</item>
            <item>Oscuro</item>
        </string-array>
        <string name="webview_theme">Tema de WebView</string>
        <string-array name="webview_theme_entries">
            <item>Por defecto del sistema</item>
            <item>Claro</item>
            <item>Oscuro</item>
        </string-array>
        <string name="wide_viewport_preference">Vista amplia</string>
        <string name="wide_viewport_summary">El uso de una vista amplia hace que el diseño de algunas páginas web se parezca más al sitio de escritorio.</string>
        <string name="display_webpage_images">Mostrar imágenes de la página web</string>
        <string name="display_webpage_images_summary">Deshabilitar para conservar ancho de banda.</string>

</resources>