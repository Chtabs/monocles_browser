<?xml version="1.0" encoding="utf-8"?>

<!--
  Copyright © 2018-2021 Arne-Brün Vogelsang <arne@monocles.de>.

  This file is part of monocles browser <https://monocles.de/more>. It is a
 * fork of Privacy Browser, which is Copyright © 2015-2021 Soren Stoutner
 * <soren@stoutner.com>.

  monocles browser is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  monocles browser is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with monocles browser.  If not, see <http://www.gnu.org/licenses/>. -->

<!-- Install location auto allows users to move monocles browser to an SD card if desired. -->
<manifest
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="de.monocles.browser"
    android:installLocation="auto" >

    <!-- Required to load websites. -->
    <uses-permission android:name="android.permission.INTERNET" />

    <!-- Required to create home screen shortcuts. -->
    <uses-permission android:name="com.android.launcher.permission.INSTALL_SHORTCUT" />


    <!-- Support Chromebooks that don't have a touch screen. -->
    <uses-feature android:name="android.hardware.touchscreen" android:required="false" />

    <!-- List the apps that monocles browser needs to query to see if they are installed. -->
    <queries>
        <!-- I2P. -->
        <package android:name="net.i2p.android.router" />

        <!-- Orbot. -->
        <package android:name="org.torproject.android" />

        <!-- OpenKeyChain. -->
        <package android:name="org.sufficientlysecure.keychain" />
    </queries>

    <!-- For API >= 23, app data is automatically backed up to Google cloud servers unless `android:allowBackup="false"` and `android:fullBackupContent="false"` is set. -->
    <application
        android:label="@string/monocles_browser"
        android:icon="@mipmap/monocles_browser"
        android:roundIcon="@mipmap/monocles_browser_round"
        android:allowBackup="false"
        android:fullBackupContent="false"
        android:supportsRtl="true"
        android:networkSecurityConfig="@xml/network_security_config"
        tools:ignore="UnusedAttribute" >

        <!-- If `android:name="android.webkit.WebView.MetricsOptOut"` is not `true` then `WebViews` will upload metrics to Google.  <https://developer.android.com/reference/android/webkit/WebView.html> -->
        <meta-data
            android:name="android.webkit.WebView.MetricsOptOut"
            android:value="true" />

        <!-- Explicitly disable "Safe Browsing". -->
        <meta-data
            android:name="android.webkit.WebView.EnableSafeBrowsing"
            android:value="false" />


        <!-- The file provider is required to encrypt files with OpenKeychain. -->
        <provider
            android:name="androidx.core.content.FileProvider"
            android:authorities="@string/file_provider"
            android:exported="false"
            android:grantUriPermissions="true" >

            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/file_provider_paths" />
        </provider>
        
        <!-- The label uses the short name so that it isn't truncated under the icon in the launcher on most phones.
            The theme has to be defined here or an ugly title bar is displayed when the app launches.
            `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:launchMode="singleTask"` makes the app launch in a new task instead of inside the task of the program that sends it an intent.
            It also makes it reuse an existing monocles browser activity if available instead of launching a new one.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.MainWebViewActivity"
            android:label="@string/short_name"
            android:theme="@style/monoclesbrowser"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:launchMode="singleTask"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" >

            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>

            <!-- Process web intents. -->
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />

                <category android:name="android.intent.category.BROWSABLE" />
                <category android:name="android.intent.category.DEFAULT" />

                <data android:scheme="http" />
                <data android:scheme="https" />
            </intent-filter>

            <!-- Process all content intents, including text, images, and MHT archives. -->
            <intent-filter>
                <action android:name="android.intent.action.VIEW" />

                <category android:name="android.intent.category.BROWSABLE" />
                <category android:name="android.intent.category.DEFAULT" />

                <data android:scheme="content" />
                <data android:mimeType="*/*" />
            </intent-filter>

            <!-- Process intents for text strings.  Sometimes URLs are presented this way. -->
            <intent-filter>
                <action android:name="android.intent.action.SEND" />

                <category android:name="android.intent.category.DEFAULT" />

                <data android:mimeType="text/plain" />
            </intent-filter>

            <!-- Process web search intents. -->
            <intent-filter>
                <action android:name="android.intent.action.WEB_SEARCH" />

                <category android:name="android.intent.category.BROWSABLE" />
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>


        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.BookmarksActivity"
            android:label="@string/bookmarks"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.BookmarksDatabaseViewActivity"
            android:label="@string/bookmarks_database_view"
            android:parentActivityName=".activities.BookmarksActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.RequestsActivity"
            android:label="@string/requests"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not reload when a bluetooth keyboard is activated/goes to sleep.
            `android:windowSoftInputMode="stateAlwaysHidden"` keeps the keyboard from displaying when the screen is rotated and after the `AddDomainDialog` is dismissed.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.DomainsActivity"
            android:label="@string/domains"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:windowSoftInputMode="stateAlwaysHidden"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.SettingsActivity"
            android:label="@string/settings"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.ImportExportActivity"
            android:label="@string/import_export"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.LogcatActivity"
            android:label="@string/logcat"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.GuideActivity"
            android:label="@string/guide"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.AboutActivity"
            android:label="@string/about_monocles_browser"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />

        <!-- `android:configChanges="orientation|screenSize"` makes the activity not restart when the orientation changes, which preserves scroll location in the WebView.
            `android:configChanges="screenLayout"` makes the activity not restart when entering or exiting split screen mode.
            `android:configChanges="keyboard|keyboardHidden"` makes the activity not restart when a bluetooth keyboard is activated/goes to sleep.
            `android:persistableMode="persistNever"` removes monocles browser from the recent apps list on a device reboot.
            `tools:ignore="unusedAttribute"` removes the lint warning that `persistableMode` does not apply to API < 21. -->
        <activity
            android:name=".activities.ViewSourceActivity"
            android:label="@string/view_source"
            android:parentActivityName=".activities.MainWebViewActivity"
            android:configChanges="orientation|screenSize|screenLayout|keyboard|keyboardHidden"
            android:screenOrientation="fullUser"
            android:persistableMode="persistNever"
            tools:ignore="UnusedAttribute" />
    </application>
</manifest>