/*
 * Copyright © 2019 Arne-Brün Vogelsang <arne@monocles.de>.
 *
 * This file is part of monocles browser <https://monocles.de/more>. It is a
 * fork of Privacy Browser, which is Copyright © 2015-2021 Soren Stoutner
 * <soren@stoutner.com>.
 *
 * monocles browser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * monocles browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with monocles browser.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.monocles.browser.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class NoSwipeViewPager extends ViewPager {
    // The basic constructor
    public NoSwipeViewPager(@NonNull Context context) {
        // Roll up to the full constructor.
        this(context, null);
    }

    // The full constructor.
    public NoSwipeViewPager(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        // Run the default commands.
        super(context, attributeSet);
    }

    // It is necessary to override `performClick()` when overriding `onTouchEvent()`
    @Override
    public boolean performClick() {
        // Run the default commands.
        super.performClick();

        // Do not consume the events.
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // `onTouchEvent()` requires calling `performClick()`.
        performClick();

        // Do not allow swiping.
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // Do not allow swiping.
        return false;
    }
}