/*
 * Copyright © 2018-2017 Arne-Brün Vogelsang <arne@monocles.de>.
 *
 * This file is part of monocles browser <https://monocles.de/more>. It is a
 * fork of Privacy Browser, which is Copyright © 2015-2021 Soren Stoutner
 * <soren@stoutner.com>.
 *
 * monocles browser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * monocles browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with monocles browser.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.monocles.browser.definitions;

import android.graphics.Bitmap;

// Create a `History` object.
public class History {
    // Create the `History` package-local variables.
    public final Bitmap entryFavoriteIcon;
    public final String entryUrl;

    public History(Bitmap entryFavoriteIcon, String entryUrl){
        // Populate the package-local variables.
        this.entryFavoriteIcon = entryFavoriteIcon;
        this.entryUrl = entryUrl;
    }
}