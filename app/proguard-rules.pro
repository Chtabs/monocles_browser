# Copyright © 2021 Arne-Brün Vogelsang <arne@monocles.de>.
#
# This file is part of monocles browser <https://monocles.de/more>. It is a
# fork of Privacy Browser, which is Copyright © 2015-2021 Soren Stoutner
# <soren@stoutner.com>.
#
# monocles browser is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# monocles browser is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with monocles browser.  If not, see <http://www.gnu.org/licenses/>.

# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ~/Android/Sdk/tools/proguard/proguard-android-optimize.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses Webview with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Make Kotlin ViewModels work correctly.
-keep class * extends androidx.lifecycle.ViewModel { *; }