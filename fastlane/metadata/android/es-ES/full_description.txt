Esta es la primera versión del navegador de monocles.

Al tratar con el navegador con la configuración correcta (restricted to Javascript and cookies), es muy fuerte para proteger su privacidad y también su seguridad!

Por favor, utilice las actualizaciones de seguridad actuales de su versión Android. En los defectos, el navegador monocles utiliza la vista web de monocles, por lo que es importante tener versiones más nuevas como Android 4.4.+.

En la configuración predeterminada, Javascript y cookies se desactivan para garantizar la mayor seguridad posible. Sin embargo, para un mejor uso de las páginas de monocles se recomienda permitir este Javascript y cookies. Simplemente agregue a los dominios de confianza en el menú. La máquina de metabús monocles no almacena su IP ni ningún otro dato.

Características:
• Bloqueo de anuncios EasyList integrado.
• Soporte para proxy de Tor Orbot.
• Fijación de certificados SSL.
• Importar/exportar la configuración y los marcadores.
